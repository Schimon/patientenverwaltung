﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Patientenverwaltung.DataBase;
using Patientenverwaltung.Dtos;
using Patientenverwaltung.Mapper.Interface;
using Patientenverwaltung.Repository.Interface;

namespace Patientenverwaltung.Repository
{
    public class PatientRepository : IPatientRepository
    {
        private readonly IPatientMapper _patientMapper;

        public PatientRepository(IPatientMapper patientMapper)
        {
            _patientMapper = patientMapper;
        }

        public async Task DeletePatient(int id)
        {
            using (var context = new PatientenverwaltungEntities())
            {
                await context.Database.ExecuteSqlCommandAsync($"DELETE FROM patient WHERE Id={id}");
            }
        }

        public async Task<IEnumerable<PatientDto>> GetAllPatientsAsync()
        {
            using (var context = new PatientenverwaltungEntities())
            {
                var patients = await context.Patients.ToListAsync();
                return patients.Select(_patientMapper.MapToDto);
            }
        }

        public async Task<PatientDto> GetPatient(int id)
        {
            using (var context = new PatientenverwaltungEntities())
            {
                var patient = await context.Patients.FirstOrDefaultAsync(x => x.Id == id);

                if (patient == null)
                {
                    throw new PatientDoesNotExistException($"The patient with id '{id}' does not exist.");
                }

                return _patientMapper.MapToDto(patient);
            }
        }

        public async Task InsertPatient(PatientDto patient)
        {
            using (var context = new PatientenverwaltungEntities())
            {
                context.Patients.Add(_patientMapper.MapToEntity(patient));
                await context.SaveChangesAsync();
            }
        }

        public async Task UpdatePatient(PatientDto patient)
        {
            using (var context = new PatientenverwaltungEntities())
            {
                var existingEntity = await context.Patients.FirstOrDefaultAsync(x => x.Id == patient.Id);

                if (existingEntity == null)
                {
                    throw new PatientDoesNotExistException($"The patient with id '{patient.Id}' does not exist.");
                }

                context.Entry(existingEntity).CurrentValues.SetValues(_patientMapper.MapToEntity(patient));
                await context.SaveChangesAsync();
            }
        }
    }
}