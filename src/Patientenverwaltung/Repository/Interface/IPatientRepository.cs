﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Patientenverwaltung.Dtos;

namespace Patientenverwaltung.Repository.Interface
{
	public interface IPatientRepository
	{
		Task DeletePatient(int id);
		Task<IEnumerable<PatientDto>> GetAllPatientsAsync();
        Task<PatientDto> GetPatient(int id);
		Task InsertPatient(PatientDto patient);
        Task UpdatePatient(PatientDto patient);
    }
}