﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Patientenverwaltung.Dtos;

namespace Patientenverwaltung.Repository.Interface
{
    public interface ITreatmentRepository
    {
        Task DeleteTreatmentAsync(int id);
        Task<IEnumerable<TreatmentDto>> GetAllTreatments();
        Task<IEnumerable<TreatmentDto>> GetAllTreatmentOfPatientAsync(int patientId);
        Task<TreatmentDto> GetTreatment(int id);
        Task InsertTreatment(TreatmentDto treatment);
        Task UpdateTreatment(TreatmentDto treatment);
        Task DeleteTreatmentByPatientIdAsync(int patientId);
    }
}