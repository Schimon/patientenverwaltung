﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Patientenverwaltung.DataBase;
using Patientenverwaltung.Dtos;
using Patientenverwaltung.Mapper.Interface;
using Patientenverwaltung.Repository.Interface;

namespace Patientenverwaltung.Repository
{
    public class TreatmentRepository : ITreatmentRepository
    {
        private readonly ITreatmentMapper _treatmentMapper;

        public TreatmentRepository(ITreatmentMapper treatmentMapper)
        {
            _treatmentMapper = treatmentMapper;
        }

        public async Task DeleteTreatmentAsync(int id)
        {
            using (var context = new PatientenverwaltungEntities())
            {
                await context.Database.ExecuteSqlCommandAsync($"DELETE FROM treatment WHERE Id={id}");
            }
        }

        public async Task<IEnumerable<TreatmentDto>> GetAllTreatments()
        {
            using (var context = new PatientenverwaltungEntities())
            {
                var treatments = await context.Treatments.ToListAsync();
                return treatments.Select(_treatmentMapper.MapToDto);
            }
        }

        public async Task<IEnumerable<TreatmentDto>> GetAllTreatmentOfPatientAsync(int patientId)
        {
            using (var context = new PatientenverwaltungEntities())
            {
                var treatments = await context.Treatments.ToListAsync();
                return treatments.Select(_treatmentMapper.MapToDto).Where(x => x.PatientId == patientId);
            }
        }

        public async Task<TreatmentDto> GetTreatment(int id)
        {
            using (var context = new PatientenverwaltungEntities())
            {
                var treatment = await context.Treatments.FirstOrDefaultAsync(x => x.Id == id);
                return _treatmentMapper.MapToDto(treatment);
            }
        }

        public async Task InsertTreatment(TreatmentDto treatment)
        {
            using (var context = new PatientenverwaltungEntities())
            {
                context.Treatments.Add(_treatmentMapper.MapToEntity(treatment));
                await context.SaveChangesAsync();
            }
        }

        public async Task UpdateTreatment(TreatmentDto treatment)
        {
            using (var context = new PatientenverwaltungEntities())
            {
                var existingEntity = await context.Treatments.FirstOrDefaultAsync(x => x.Id == treatment.Id);

                if (existingEntity == null)
                {
                    throw new TreatmentDoesNotExistException($"The patient with id '{treatment.Id}' does not exist.");
                }

                context.Entry(existingEntity).CurrentValues.SetValues(_treatmentMapper.MapToEntity(treatment));
                await context.SaveChangesAsync();
            }
        }

        public async Task DeleteTreatmentByPatientIdAsync(int patientId)
        {
            using (var context = new PatientenverwaltungEntities())
            {
                await context.Database.ExecuteSqlCommandAsync($"DELETE FROM treatment WHERE PatientId={patientId}");
            }
        }
    }
}