﻿using System;

namespace Patientenverwaltung.Repository
{
    public class PatientDoesNotExistException : Exception
    {
        public PatientDoesNotExistException()
        {
        }

        public PatientDoesNotExistException(string message) 
            : base(message)
        {
        }

        public PatientDoesNotExistException(string message, Exception innerException) 
            : base(message, innerException)
        {
        }
    }
}