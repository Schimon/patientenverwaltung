﻿using System;

namespace Patientenverwaltung.Repository
{
    public class TreatmentDoesNotExistException : Exception
    {
        public TreatmentDoesNotExistException()
        {
        }

        public TreatmentDoesNotExistException(string message) 
            : base(message)
        {
        }

        public TreatmentDoesNotExistException(string message, Exception innerException) 
            : base(message,innerException)
        {
        }
    }
}