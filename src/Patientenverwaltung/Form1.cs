﻿using Ninject;
using Patientenverwaltung.DependencyInjection;
using Patientenverwaltung.Dtos;
using Patientenverwaltung.Enums;
using Patientenverwaltung.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Patientenverwaltung.Dialogs;
using Patientenverwaltung.Repository;
using Patientenverwaltung.Services.Interface;

namespace Patientenverwaltung
{
	public partial class Form1 : Form
	{

		private readonly IGridPatientMapper _gridPatientMapper;
		private readonly IPatientService _patientService;
		private readonly ISearchService _searchService;

        private IEnumerable<PatientDto> _patientsCache;

		public Form1(
			IGridPatientMapper gridPatientMapper, 
			IPatientService patientService, 
			ISearchService searchService)
		{
			_gridPatientMapper = gridPatientMapper;
			_patientService = patientService;
			_searchService = searchService;

            InitializeComponent();
            InitTooltips();
        }

        public async Task InitializeAsync()
        {
	        await LoadPatientsAndUpdateGridAsync();
			
        }

		public async Task LoadPatientsAndUpdateGridAsync()
		{
			var patients = (await _patientService.GetAllPatientsWithTreatmentsAsync()).ToList();
			UpdateGrid(patients);

			_patientsCache = patients;
		}

		private void InitTooltips()
		{
			if (components == null)
				components = new System.ComponentModel.Container();

            btnNewPatient.SetToolTip(components, "Patient hinzufügen", "Klicken um neuen Patienten anzulegen");
			btnEdit.SetToolTip(components, "Patienten bearbeiten", "Klicke um Patienten zu berarbeiten");
			btnDelete.SetToolTip(components, "Patienten löschen", "Klicken um Patienten zu löschen");
			comboBox1.SetToolTip(components, "Suche filtern", "Auswahl der Suchfilter");
		}

		private void btnNewPatient_Click(object sender, EventArgs e)
		{
			labelError.Text = "";
			OpenEditPatientForm(null);
        }

		private async void btnDelete_Click(object sender, EventArgs e)
		{
			labelError.Text = "";
			var gridPatient = GetSelectedPatient();

			if (gridPatient == null)
			{
				labelError.Text = "Wählen Sie einen Patienten aus";
				return;
			}


			var dialogResult = MessageBox.Show($"Möchten Sie wirklich den Patient \"{gridPatient.Nachname} {gridPatient.Vorname}\" löschen?", "Patient löschen?", MessageBoxButtons.YesNo);
			if (dialogResult == DialogResult.Yes)
			{
				await _patientService.DeletePatientAsync(gridPatient.Id);
				await LoadPatientsAndUpdateGridAsync();
			}

		}

		private async void btnEdit_Click(object sender, EventArgs e)
		{
			labelError.Text = "";
			var gridPatient = GetSelectedPatient();

			if(gridPatient == null)
			{
				labelError.Text = "Wählen Sie einen Patienten aus";
				return;
			}

            try
            {
                var patientToEdit = await _patientService.GetPatientAsync(gridPatient.Id);
                OpenEditPatientForm(patientToEdit);
            }
            catch (PatientDoesNotExistException)
            {
                labelError.Text = "Der ausgewählte Patient existiert nicht mehr.";
            }
            catch (Exception ex)
            {
                labelError.Text = $"Ein unerwarteter Fehler ist aufgetreten: {ex.Message}";
            }

		}

		private void OpenEditPatientForm(PatientDto patient)
		{
			var form = new Ninject.Parameters.ConstructorArgument("mainForm", this);
			var p = new Ninject.Parameters.ConstructorArgument("patient", patient);

			var patientForm = KernelLocator.Kernel.Get<FormPatient>(form, p);

			patientForm.Show();
		}

		private GridPatient GetSelectedPatient()
		{
			if (dataGridView.SelectedRows.Count != 1)
				return null;

			return (GridPatient) dataGridView.SelectedRows[0].DataBoundItem;
		}

		private void UpdateGrid(IEnumerable<PatientDto> patientEnumerable)
		{
			var patientList = patientEnumerable.ToList();
            var gridPatients = patientList.Select(x => _gridPatientMapper.GetGridPatient(x)).ToList();
			dataGridView.DataSource = gridPatients;
			dataGridView.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
        }

		private void textBoxSuche_TextChanged(object sender, EventArgs e)
		{
			Search();
		}
		private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			Search();
		}

		private void Search()
		{
			var searchString = textBoxSuche.Text;

			UpdateGrid(_searchService.Search(_patientsCache, searchString, (SearchTypes)Enum.Parse(typeof(SearchTypes), comboBox1.Text)));
		}
	}
}
