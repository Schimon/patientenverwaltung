﻿using Ninject;

namespace Patientenverwaltung.DependencyInjection
{
	public class KernelLocator
	{
		public static IKernel Kernel { get; set; }
	}
}
