﻿using Ninject.Modules;
using Patientenverwaltung.Mapper;
using Patientenverwaltung.Mapper.Interface;
using Patientenverwaltung.Repository;
using Patientenverwaltung.Repository.Interface;
using Patientenverwaltung.Services;
using Patientenverwaltung.Services.Interface;

namespace Patientenverwaltung.DependencyInjection
{
	public class PatientenModule : NinjectModule
	{
		public override void Load()
		{
			Bind<IGridPatientMapper>().To<GridPatientMapper>();
			Bind<ISearchService>().To<SearchService>();
			Bind<IPatientRepository>().To<PatientRepository>();
            Bind<IPatientMapper>().To<PatientMapper>();
            Bind<ITreatmentRepository>().To<TreatmentRepository>();
            Bind<ITreatmentMapper>().To<TreatmentMapper>();
			Bind<IPatientService>().To<PatientService>();
		}
	}
}
