﻿using System.ComponentModel;
using System.Windows.Forms;

namespace Patientenverwaltung.Extensions
{
	public static class ToolTipExtension
	{
		public static void SetToolTip(this Control control, IContainer components, string title, string text)
		{
			var tooltip = new ToolTip(components)
			{
				ToolTipTitle = title
			};

			tooltip.SetToolTip(control, text);
		}
	}
}
