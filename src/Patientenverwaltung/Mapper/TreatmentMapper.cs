﻿using Patientenverwaltung.DataBase;
using Patientenverwaltung.Dtos;
using Patientenverwaltung.Mapper.Interface;

namespace Patientenverwaltung.Mapper
{
    public class TreatmentMapper : ITreatmentMapper
    {
        public TreatmentDto MapToDto(Treatment treatment)
        {
            return new TreatmentDto
            {
                Id = treatment.Id,
                PatientId = treatment.PatientId,
                Date = treatment.Date,
                Misc = treatment.Misc,
                Checkup = treatment.Checkup
            };
        }

        public Treatment MapToEntity(TreatmentDto treatment)
        {
            return new Treatment
            {
                PatientId = treatment.PatientId,
                Id = treatment.Id,
                Date = treatment.Date,
                Misc = treatment.Misc,
                Checkup = treatment.Checkup
            };
        }
    }
}