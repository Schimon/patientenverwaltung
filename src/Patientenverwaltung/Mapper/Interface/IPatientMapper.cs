﻿using Patientenverwaltung.Dtos;
using Patientenverwaltung.DataBase;

namespace Patientenverwaltung.Mapper.Interface
{
    public interface IPatientMapper
    {
        PatientDto MapToDto(Patient entity);
        Patient MapToEntity(PatientDto model);
    }
}
