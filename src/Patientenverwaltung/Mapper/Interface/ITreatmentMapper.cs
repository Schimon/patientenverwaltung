﻿using Patientenverwaltung.DataBase;
using Patientenverwaltung.Dtos;

namespace Patientenverwaltung.Mapper.Interface
{
    public interface ITreatmentMapper
    {
        TreatmentDto MapToDto(Treatment treatment);
        Treatment MapToEntity(TreatmentDto treatment);
    }
}