﻿using Patientenverwaltung.DataBase;
using Patientenverwaltung.Dtos;
using Patientenverwaltung.Enums;
using Patientenverwaltung.Mapper.Interface;
using System;

namespace Patientenverwaltung.Mapper
{
    public class PatientMapper : IPatientMapper
    {
        public PatientDto MapToDto(Patient entity)
        {
            return new PatientDto
            {
                Address = new Address
                {
                    StreetNumber = entity.StreetNumber,
                    City = entity.City,
                    Postalcode = entity.Postalcode,
                    Street = entity.Street,
                    Country = entity.Country
                },
                Salutation = (Salutation)Enum.Parse(typeof(Salutation),entity.Salutation),
                Description = entity.Description,
                DateOfBirth = entity.DateOfBirth,
                Id = entity.Id,
                Insurance = entity.Insurance,
                Name = entity.Name,
                Surname = entity.Surname,
                Phone = entity.Phone,
                InsuranceNumber = entity.InsuranceNumber,
            };
        }

        public Patient MapToEntity(PatientDto model)
        {
            return new Patient
            {
                City = model.Address.City,
                Country = model.Address.Country,
                DateOfBirth = model.DateOfBirth,
                Description = model.Description,
                Id = model.Id,
                Insurance = model.Insurance,
                InsuranceNumber = model.InsuranceNumber,
                Name = model.Name,
                Surname = model.Surname,
                Phone = model.Phone,
                Postalcode = model.Address.Postalcode,
                Salutation = model.Salutation.ToString(),
                Street = model.Address.Street,
                StreetNumber = model.Address.StreetNumber
            };
        }
    }
}
