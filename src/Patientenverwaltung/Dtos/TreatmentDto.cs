﻿using System;

namespace Patientenverwaltung.Dtos
{
	public class TreatmentDto
	{
	    public int Id { get; set; }
        public int PatientId { get; set; }
        public DateTime Date { get; set; }
	    public string Checkup { get; set; }
	    public string Misc { get; set; }
	}
}
