﻿using Patientenverwaltung.Enums;
using System;
using System.Collections.Generic;

namespace Patientenverwaltung.Dtos
{
	public class PatientDto
	{
		public string Insurance { get; set; }
	    public string InsuranceNumber { get; set; }
	    public Salutation Salutation { get; set; }
	    public string Surname { get; set; }
	    public string Name { get; set; }
	    public Address Address { get; set; }
	    public DateTime DateOfBirth { get; set; }
	    public string Phone { get; set; }
	    public string Description { get; set; }
	    public List<TreatmentDto> Treatments { get; set; }
	    public int Id { get; set; }
	}
}
