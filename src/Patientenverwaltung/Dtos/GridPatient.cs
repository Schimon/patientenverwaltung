﻿using System;

namespace Patientenverwaltung.Dtos
{
	public class GridPatient
	{
		public int Id { get; set; }
		public string Surname { get; set; }
		public string Name { get; set; }
		public string DateOfBirth { get; set; }
		public string City { get; set; }
		public string Insurance { get; set; }
		public DateTime? LastTreatment { get; set; }
	}
}
