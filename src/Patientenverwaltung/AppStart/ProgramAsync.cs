﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ninject;
using Patientenverwaltung.DependencyInjection;

namespace Patientenverwaltung.AppStart
{
    public class ProgramAsync
    {
        private readonly Form1 _mainForm;
        public EventHandler<EventArgs> ExitRequested;

        public ProgramAsync()
        {
            KernelLocator.Kernel = new StandardKernel(new PatientenModule());
            _mainForm = KernelLocator.Kernel.Get<Form1>();
            _mainForm.FormClosed += MainFormClosed;
        }

        public void MainFormClosed(object sender, FormClosedEventArgs e)
        {
            OnExitRequested(EventArgs.Empty);
        }

        public async Task StartAsync()
        {
            await _mainForm.InitializeAsync();
            _mainForm.Show();
        }

        protected virtual void OnExitRequested(EventArgs e)
        {
            ExitRequested?.Invoke(this, e);
        }
    }
}