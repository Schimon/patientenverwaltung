﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Patientenverwaltung.AppStart;

namespace Patientenverwaltung
{
	static class Program
	{
		/// <summary>
		/// Der Haupteinstiegspunkt für die Anwendung.
		/// </summary>
		[STAThread]
		static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            SynchronizationContext.SetSynchronizationContext(new WindowsFormsSynchronizationContext());

            ProgramAsync prog = new ProgramAsync();
            prog.ExitRequested += prog_ExitRequested;
            Task programStart = prog.StartAsync();
            HandleExceptions(programStart);
            Application.Run();

        }

        static void prog_ExitRequested(object sender, EventArgs e)
        {
            Application.ExitThread();
        }

        private static async void HandleExceptions(Task task)
        {
            try
            {
                await Task.Yield();
                await task;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Application.Exit();
            }
        }
    }
}
