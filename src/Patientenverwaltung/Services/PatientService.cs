﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Patientenverwaltung.Dtos;
using Patientenverwaltung.Repository.Interface;
using Patientenverwaltung.Services.Interface;

namespace Patientenverwaltung.Services
{
	public class PatientService : IPatientService
	{
		private readonly IPatientRepository _patientRepository;
		private readonly ITreatmentRepository _treatmentRepository;

		public PatientService(IPatientRepository patientRepository, ITreatmentRepository treatmentRepository)
		{
			_patientRepository = patientRepository;
			_treatmentRepository = treatmentRepository;
		}

		public async Task<IEnumerable<PatientDto>> GetAllPatients()
		{
			return await _patientRepository.GetAllPatientsAsync();
		}

		public async Task<IEnumerable<PatientDto>> GetAllPatientsWithTreatmentsAsync()
		{
			var patients = (await _patientRepository.GetAllPatientsAsync()).ToList();

			foreach (var patient in patients)
			{
				patient.Treatments = (await _treatmentRepository.GetAllTreatmentOfPatientAsync(patient.Id)).ToList();
			}

			return patients;
		}

		public async Task<PatientDto> GetPatientAsync(int patientId)
		{
			var patient = await _patientRepository.GetPatient(patientId);

			if (patient == null)
			{
				return null;
			}

			patient.Treatments = (await _treatmentRepository.GetAllTreatmentOfPatientAsync(patient.Id)).ToList();
			return patient;
		}

		public async Task DeletePatientAsync(int patientId)
        {
            await _treatmentRepository.DeleteTreatmentByPatientIdAsync(patientId);
			await _patientRepository.DeletePatient(patientId);
		}

		public async Task AddPatientAsync(PatientDto patient)
		{
			await _patientRepository.InsertPatient(patient);
		}

		public async Task UpdatePatientAsync(PatientDto patient)
		{
			await _patientRepository.UpdatePatient(patient);
		}

	}
}