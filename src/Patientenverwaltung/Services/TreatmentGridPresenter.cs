﻿using Patientenverwaltung.Dtos;
using Patientenverwaltung.Services.Interface;

namespace Patientenverwaltung.Services
{
    public class TreatmentGridPresenter : ITreatmentGridPresenter
    {

        public GridTreatment GetGridTreatment(TreatmentDto treatment)
        {
            return new GridTreatment
            {
                Date = treatment.Date,
                Checkup = treatment.Checkup,
                Misc = treatment.Misc
            };
        }
    }
}