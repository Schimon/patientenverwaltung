﻿using System;
using System.Linq;
using Patientenverwaltung.Dtos;
using Patientenverwaltung.Services.Interface;

namespace Patientenverwaltung.Services
{
	public class GridPatientMapper : IGridPatientMapper
	{
        public GridPatient GetGridPatient(PatientDto patient)
		{
			return new GridPatient
			{
				Id = patient.Id,
				Nachname = patient.Surname,
				Vorname = patient.Name,
				Geburtsdatum = patient.DateOfBirth.ToString("dd.MM.yyyy"),
				Versicherung = patient.Insurance,
				Ort = patient.Address.City,
				LetzteBehandlung = GetLastTreatment(patient)
			};
		}

		private DateTime? GetLastTreatment(PatientDto patient)
		{
			var treatmentsInPast = patient.Treatments.Where(x => x.Date < DateTime.Now).ToList();

			if (!treatmentsInPast.Any())
				return null;

			return treatmentsInPast.OrderByDescending(x => x.Date).First().Date;
		}

	}
}
