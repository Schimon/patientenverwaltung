﻿using Patientenverwaltung.Dtos;
using Patientenverwaltung.Enums;
using Patientenverwaltung.Services.Interface;
using System.Collections.Generic;
using System.Linq;

namespace Patientenverwaltung.Services
{
	public class SearchService : ISearchService
	{

		public IEnumerable<PatientDto> Search(IEnumerable<PatientDto> patientList, string searchParameter, SearchTypes searchType)
		{
			searchParameter = searchParameter.ToLower();

			switch(searchType)
			{
				case SearchTypes.Id:
					return SearchAfterId(patientList, searchParameter);
				case SearchTypes.Surname:
					return SearchAfterSurname(patientList, searchParameter);
				case SearchTypes.Name:
					return SearchAfterName(patientList, searchParameter);
				default:
					return SearchAll(patientList, searchParameter);
			}
		}

		private IEnumerable<PatientDto> SearchAll(IEnumerable<PatientDto> patientList, string searchParameter)
		{
			return SearchAfterId(patientList, searchParameter)
				.Concat(SearchAfterFullName(patientList, searchParameter))
				.Concat(SearchAfterSurname(patientList, searchParameter))
				.Concat(SearchAfterName(patientList, searchParameter))
				.GroupBy(x => x.Id)
				.Select(x => x.First());
		}

		private IEnumerable<PatientDto> SearchAfterId(IEnumerable<PatientDto> patientList, string searchParameter)
		{
			return patientList.Where(x => x.Id.ToString().StartsWith(searchParameter)).ToList();
		}

		private IEnumerable<PatientDto> SearchAfterSurname(IEnumerable<PatientDto> patientList, string searchParameter)
		{
			return patientList.Where(x => x.Surname.ToLower().StartsWith(searchParameter)).ToList();
		}

		private IEnumerable<PatientDto> SearchAfterName(IEnumerable<PatientDto> patientList, string searchParameter)
		{
			return patientList.Where(x => x.Name.ToLower().StartsWith(searchParameter)).ToList();
		}

		private IEnumerable<PatientDto> SearchAfterFullName(IEnumerable<PatientDto> patientList, string searchParameter)
		{
			searchParameter = searchParameter.Replace(" ", "");

            var toReturn = new List<PatientDto>();
			foreach(var patient in patientList)
			{
				var surnameName = (patient.Surname + patient.Name + patient.Surname).ToLower();

				if (surnameName.Contains(searchParameter))
					toReturn.Add(patient);
			}

			return toReturn;
		}

	}
}
