﻿using System.Collections.Generic;
using Patientenverwaltung.Dtos;
using Patientenverwaltung.Enums;

namespace Patientenverwaltung.Services.Interface
{
	public interface ISearchService
	{
		IEnumerable<PatientDto> Search(IEnumerable<PatientDto> patientList, string searchParameter, SearchTypes searchType);
	}
}