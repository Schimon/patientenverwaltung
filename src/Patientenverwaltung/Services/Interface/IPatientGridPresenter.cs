﻿using System.Threading.Tasks;
using Patientenverwaltung.Dtos;

namespace Patientenverwaltung.Services.Interface
{
	public interface IGridPatientMapper
	{
		GridPatient GetGridPatient(PatientDto patient);
	}
}