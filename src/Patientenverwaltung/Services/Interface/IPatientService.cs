﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Patientenverwaltung.Dtos;

namespace Patientenverwaltung.Services.Interface
{
	public interface IPatientService
	{
		Task<IEnumerable<PatientDto>> GetAllPatients();
		Task<IEnumerable<PatientDto>> GetAllPatientsWithTreatmentsAsync();
		Task<PatientDto> GetPatientAsync(int patientId);
		Task DeletePatientAsync(int patientId);
		Task AddPatientAsync(PatientDto patient);
		Task UpdatePatientAsync(PatientDto patient);
	}
}