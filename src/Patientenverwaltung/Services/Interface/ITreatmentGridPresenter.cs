﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Patientenverwaltung.Dtos;

namespace Patientenverwaltung.Services.Interface
{
    public interface ITreatmentGridPresenter
    {
        GridTreatment GetGridTreatment(TreatmentDto treatment);
    }
}