﻿namespace Patientenverwaltung.Enums
{
	public enum SearchTypes
	{
		All = 0,
		Id = 1,
		Surname = 2,
		Name = 3
	}
}
