﻿namespace Patientenverwaltung.Enums
{
	public enum Salutation
	{
		Herr = 0,
		Frau = 1
	}
}