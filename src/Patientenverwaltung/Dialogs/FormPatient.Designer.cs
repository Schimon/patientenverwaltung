﻿using System.Windows.Forms;

namespace Patientenverwaltung.Dialogs
{
    partial class FormPatient
	{
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormPatient));
            this.labKrankenkasse = new System.Windows.Forms.Label();
            this.txtKrankenkasse = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtVersicherungsnummer = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtVorname = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNachname = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtStrasse = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTelefon = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtBesonderheit = new System.Windows.Forms.TextBox();
            this.txtPlz = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.txtHausNummer = new System.Windows.Forms.TextBox();
            this.txtOrt = new System.Windows.Forms.TextBox();
            this.btnSpeichern = new System.Windows.Forms.Button();
            this.dataGridViewBehandlungen = new System.Windows.Forms.DataGridView();
            this.btnDelete = new System.Windows.Forms.Button();
            this.labelError = new System.Windows.Forms.Label();
            this.btnEdit = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxAnrede = new System.Windows.Forms.ComboBox();
            this.dtpGeburtsdatum = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBehandlungen)).BeginInit();
            this.SuspendLayout();
            // 
            // labKrankenkasse
            // 
            this.labKrankenkasse.AutoSize = true;
            this.labKrankenkasse.Location = new System.Drawing.Point(36, 195);
            this.labKrankenkasse.Name = "labKrankenkasse";
            this.labKrankenkasse.Size = new System.Drawing.Size(76, 13);
            this.labKrankenkasse.TabIndex = 0;
            this.labKrankenkasse.Text = "Versicherung *";
            // 
            // txtKrankenkasse
            // 
            this.txtKrankenkasse.Location = new System.Drawing.Point(153, 195);
            this.txtKrankenkasse.Name = "txtKrankenkasse";
            this.txtKrankenkasse.Size = new System.Drawing.Size(175, 20);
            this.txtKrankenkasse.TabIndex = 8;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(347, 125);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Versicherungs Nr. *";
            // 
            // txtVersicherungsnummer
            // 
            this.txtVersicherungsnummer.Location = new System.Drawing.Point(442, 122);
            this.txtVersicherungsnummer.Name = "txtVersicherungsnummer";
            this.txtVersicherungsnummer.Size = new System.Drawing.Size(123, 20);
            this.txtVersicherungsnummer.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(36, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Nachname *";
            // 
            // txtVorname
            // 
            this.txtVorname.Location = new System.Drawing.Point(153, 67);
            this.txtVorname.Name = "txtVorname";
            this.txtVorname.Size = new System.Drawing.Size(175, 20);
            this.txtVorname.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(36, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Vorname *";
            // 
            // txtNachname
            // 
            this.txtNachname.Location = new System.Drawing.Point(153, 96);
            this.txtNachname.Name = "txtNachname";
            this.txtNachname.Size = new System.Drawing.Size(175, 20);
            this.txtNachname.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(36, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Straße - Hausnr. *";
            // 
            // txtStrasse
            // 
            this.txtStrasse.Location = new System.Drawing.Point(153, 130);
            this.txtStrasse.Name = "txtStrasse";
            this.txtStrasse.Size = new System.Drawing.Size(124, 20);
            this.txtStrasse.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(347, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Geburtsdaturm *";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(347, 67);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Telefon";
            // 
            // txtTelefon
            // 
            this.txtTelefon.Location = new System.Drawing.Point(442, 64);
            this.txtTelefon.Name = "txtTelefon";
            this.txtTelefon.Size = new System.Drawing.Size(123, 20);
            this.txtTelefon.TabIndex = 10;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(347, 96);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Beschreibung";
            // 
            // txtBesonderheit
            // 
            this.txtBesonderheit.Location = new System.Drawing.Point(442, 93);
            this.txtBesonderheit.Name = "txtBesonderheit";
            this.txtBesonderheit.Size = new System.Drawing.Size(123, 20);
            this.txtBesonderheit.TabIndex = 11;
            // 
            // txtPlz
            // 
            this.txtPlz.Location = new System.Drawing.Point(152, 166);
            this.txtPlz.Name = "txtPlz";
            this.txtPlz.Size = new System.Drawing.Size(62, 20);
            this.txtPlz.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(35, 166);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "PLZ - Stadt *";
            // 
            // btnAdd
            // 
            this.btnAdd.Image = global::Patientenverwaltung.Properties.Resources.ic_add;
            this.btnAdd.Location = new System.Drawing.Point(536, 251);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(35, 23);
            this.btnAdd.TabIndex = 14;
            this.btnAdd.Text = "+";
            this.btnAdd.UseVisualStyleBackColor = true;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // txtHausNummer
            // 
            this.txtHausNummer.Location = new System.Drawing.Point(282, 130);
            this.txtHausNummer.Name = "txtHausNummer";
            this.txtHausNummer.Size = new System.Drawing.Size(45, 20);
            this.txtHausNummer.TabIndex = 5;
            // 
            // txtOrt
            // 
            this.txtOrt.Location = new System.Drawing.Point(220, 166);
            this.txtOrt.Name = "txtOrt";
            this.txtOrt.Size = new System.Drawing.Size(107, 20);
            this.txtOrt.TabIndex = 7;
            // 
            // btnSpeichern
            // 
            this.btnSpeichern.Location = new System.Drawing.Point(348, 166);
            this.btnSpeichern.Name = "btnSpeichern";
            this.btnSpeichern.Size = new System.Drawing.Size(217, 49);
            this.btnSpeichern.TabIndex = 13;
            this.btnSpeichern.Text = "Speichern";
            this.btnSpeichern.UseVisualStyleBackColor = true;
            this.btnSpeichern.Click += new System.EventHandler(this.btnSpeichern_Click);
            // 
            // dataGridViewBehandlungen
            // 
            this.dataGridViewBehandlungen.AllowUserToResizeRows = false;
            this.dataGridViewBehandlungen.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewBehandlungen.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewBehandlungen.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dataGridViewBehandlungen.Location = new System.Drawing.Point(12, 251);
            this.dataGridViewBehandlungen.MultiSelect = false;
            this.dataGridViewBehandlungen.Name = "dataGridViewBehandlungen";
            this.dataGridViewBehandlungen.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewBehandlungen.Size = new System.Drawing.Size(512, 206);
            this.dataGridViewBehandlungen.TabIndex = 15;
            // 
            // btnDelete
            // 
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Location = new System.Drawing.Point(538, 428);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(33, 29);
            this.btnDelete.TabIndex = 16;
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // labelError
            // 
            this.labelError.AutoSize = true;
            this.labelError.ForeColor = System.Drawing.Color.Red;
            this.labelError.Location = new System.Drawing.Point(12, 463);
            this.labelError.Name = "labelError";
            this.labelError.Size = new System.Drawing.Size(0, 13);
            this.labelError.TabIndex = 17;
            // 
            // btnEdit
            // 
            this.btnEdit.Image = ((System.Drawing.Image)(resources.GetObject("btnEdit.Image")));
            this.btnEdit.Location = new System.Drawing.Point(538, 393);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(33, 29);
            this.btnEdit.TabIndex = 15;
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(37, 31);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 19;
            this.label8.Text = "Anrede";
            // 
            // comboBoxAnrede
            // 
            this.comboBoxAnrede.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxAnrede.FormattingEnabled = true;
            this.comboBoxAnrede.Location = new System.Drawing.Point(153, 28);
            this.comboBoxAnrede.Name = "comboBoxAnrede";
            this.comboBoxAnrede.Size = new System.Drawing.Size(175, 21);
            this.comboBoxAnrede.TabIndex = 1;
            // 
            // dtpGeburtsdatum
            // 
            this.dtpGeburtsdatum.CustomFormat = "dd.MM.yyyy";
            this.dtpGeburtsdatum.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpGeburtsdatum.Location = new System.Drawing.Point(442, 36);
            this.dtpGeburtsdatum.Name = "dtpGeburtsdatum";
            this.dtpGeburtsdatum.Size = new System.Drawing.Size(123, 20);
            this.dtpGeburtsdatum.TabIndex = 9;
            // 
            // FormPatient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(583, 485);
            this.Controls.Add(this.dtpGeburtsdatum);
            this.Controls.Add(this.comboBoxAnrede);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.labelError);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.dataGridViewBehandlungen);
            this.Controls.Add(this.btnSpeichern);
            this.Controls.Add(this.txtOrt);
            this.Controls.Add(this.txtHausNummer);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.txtPlz);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtBesonderheit);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtTelefon);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtStrasse);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtNachname);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtVorname);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtVersicherungsnummer);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtKrankenkasse);
            this.Controls.Add(this.labKrankenkasse);
            this.Name = "FormPatient";
            this.Text = "Patient";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewBehandlungen)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labKrankenkasse;
        private System.Windows.Forms.TextBox txtKrankenkasse;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtVersicherungsnummer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtVorname;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtNachname;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtStrasse;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtTelefon;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtBesonderheit;
        private System.Windows.Forms.TextBox txtPlz;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnAdd;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
		private System.Windows.Forms.TextBox txtHausNummer;
		private System.Windows.Forms.TextBox txtOrt;
		private System.Windows.Forms.Button btnSpeichern;
		private System.Windows.Forms.DataGridView dataGridViewBehandlungen;
		private System.Windows.Forms.Button btnDelete;
		private System.Windows.Forms.Label labelError;
		private System.Windows.Forms.Button btnEdit;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.ComboBox comboBoxAnrede;
		private DateTimePicker dtpGeburtsdatum;
	}
}

