﻿using Patientenverwaltung.Extensions;

namespace Patientenverwaltung.Dialogs
{
    partial class FormTreatment
	{
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxSonstiges = new System.Windows.Forms.TextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.calendar = new System.Windows.Forms.MonthCalendar();
            this.textBoxBehandlung = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Datum";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(221, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Bezeichnung";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(221, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Beschreibung";
            // 
            // textBoxSonstiges
            // 
            this.textBoxSonstiges.Location = new System.Drawing.Point(224, 121);
            this.textBoxSonstiges.Multiline = true;
            this.textBoxSonstiges.Name = "textBoxSonstiges";
            this.textBoxSonstiges.Size = new System.Drawing.Size(321, 99);
            this.textBoxSonstiges.TabIndex = 5;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(554, 196);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 24);
            this.btnSave.TabIndex = 6;
            this.btnSave.Text = "Speichern";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // calendar
            // 
            this.calendar.Location = new System.Drawing.Point(18, 58);
            this.calendar.MaxSelectionCount = 1;
            this.calendar.Name = "calendar";
            this.calendar.TabIndex = 7;
            // 
            // textBoxBehandlung
            // 
            this.textBoxBehandlung.Location = new System.Drawing.Point(224, 62);
            this.textBoxBehandlung.Name = "textBoxBehandlung";
            this.textBoxBehandlung.Size = new System.Drawing.Size(321, 20);
            this.textBoxBehandlung.TabIndex = 8;
            // 
            // FormTreatment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 236);
            this.Controls.Add(this.textBoxBehandlung);
            this.Controls.Add(this.calendar);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.textBoxSonstiges);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FormTreatment";
            this.Text = "TreatmentDto";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxSonstiges;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.MonthCalendar calendar;
		private System.Windows.Forms.TextBox textBoxBehandlung;
	}
}