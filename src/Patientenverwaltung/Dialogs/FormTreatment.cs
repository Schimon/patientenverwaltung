﻿using System.Threading.Tasks;
using System.Windows.Forms;
using Patientenverwaltung.Dtos;

namespace Patientenverwaltung.Dialogs
{
	public partial class FormTreatment : Form
    {

		private TreatmentDto _treatmentDto;
		private readonly FormPatient _formPatient;

        public FormTreatment(FormPatient formPatient, TreatmentDto treatmentDto)
        {
            InitializeComponent();

			_formPatient = formPatient;
			_treatmentDto = treatmentDto;

            if (_treatmentDto == null)
                return;

            calendar.SelectionStart = _treatmentDto.Date;
            textBoxSonstiges.Text = _treatmentDto.Misc;
            textBoxBehandlung.Text = _treatmentDto.Checkup;
        }

		private async void btnSave_Click(object sender, System.EventArgs e)
		{
			if (_treatmentDto == null)
            {
                _treatmentDto = new TreatmentDto();
			}

			_treatmentDto.Date = calendar.SelectionStart;
			_treatmentDto.Misc = textBoxSonstiges.Text;
			_treatmentDto.Checkup = textBoxBehandlung.Text;

			await _formPatient.SaveTreatment(_treatmentDto);
			_formPatient.UpdateGrid();

			this.Close();

		}
	}
}
