﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Patientenverwaltung.Dtos;
using Patientenverwaltung.Enums;
using Patientenverwaltung.Extensions;
using Patientenverwaltung.Repository.Interface;
using Patientenverwaltung.Services.Interface;

// ReSharper disable LocalizableElement
namespace Patientenverwaltung.Dialogs
{
    public partial class FormPatient : Form
    {

		private PatientDto _patient;
		private readonly IPatientService _patientService;
        private readonly ITreatmentRepository _treatmentRepository;
        private readonly Form1 _mainForm;

        public FormPatient(Form1 mainForm, PatientDto patient, IPatientService patientService, ITreatmentRepository treatmentRepository)
        {
            InitializeComponent();
			InitTooltips();

			_mainForm = mainForm;
			_patient = patient;
			_patientService = patientService;
            _treatmentRepository = treatmentRepository;

            FillTextBoxes();
			UpdateGrid();
        }

		private void InitTooltips()
		{
			if (components == null)
				components = new System.ComponentModel.Container();
			btnSpeichern.SetToolTip(components, "Patient speichern", "Klicken um Patienten zu speichern");
			btnDelete.SetToolTip(components, "Behandlung löschen", "Klicken um ausgewählte Behandlung zu löschen");
			btnEdit.SetToolTip(components, "Behandlung bearbeiten", "Klicken um ausgewählte Behandlung zu löschen");
			btnAdd.SetToolTip(components, "Behandlung hinzufügen", "Klicken um eine neue Behandlung hinzuzufügen");
			comboBoxAnrede.SetToolTip(components, "Anrede auswählen", "Klicken um eine andere Anrede auszuwählen");

		}

		private void FillTextBoxes()
		{
			comboBoxAnrede.DataSource = Enum.GetValues(typeof(Salutation));
			
			if (_patient?.Address == null)
				return;

			var address = _patient.Address;

			txtKrankenkasse.Text = _patient.Insurance;
			txtVersicherungsnummer.Text = _patient.InsuranceNumber;
			txtVorname.Text = _patient.Surname;
			txtNachname.Text = _patient.Name;
			txtStrasse.Text = address.Street;
			txtHausNummer.Text = address.StreetNumber;
			txtPlz.Text = address.Postalcode;
			txtOrt.Text = address.City;
			dtpGeburtsdatum.Value = _patient.DateOfBirth;
			txtTelefon.Text = _patient.Phone;
			txtBesonderheit.Text = _patient.Description;
			comboBoxAnrede.SelectedIndex = (int) _patient.Salutation;
			comboBoxAnrede.Update();
		}

		private async void btnSpeichern_Click(object sender, EventArgs e)
		{
			ClearErrorMessage();

			if (!ValidateForm())
			{
				labelError.Text = "Es sind nicht alle benötigten Felder ausgefüllt";
				return;
			}


			await SavePatientAsync();
			await _mainForm.LoadPatientsAndUpdateGridAsync();

			Close();
        }

		public async Task SaveTreatment(TreatmentDto treatmentDto)
		{
			var existingTreatment = _patient.Treatments.FirstOrDefault(x => x.Id == treatmentDto.Id);

			if(existingTreatment == null)
            {
                treatmentDto.PatientId = _patient.Id;
                await _treatmentRepository.InsertTreatment(treatmentDto);
	            _patient.Treatments = (await _treatmentRepository.GetAllTreatmentOfPatientAsync(_patient.Id)).ToList();
            }
            else
            {
                existingTreatment.Checkup = treatmentDto.Checkup;
                existingTreatment.Date = treatmentDto.Date;
                existingTreatment.Misc = treatmentDto.Misc;
	            await _treatmentRepository.UpdateTreatment(existingTreatment);
            }

			await _mainForm.LoadPatientsAndUpdateGridAsync();
		}

		public async Task SavePatientAsync()
		{
			var existingTreatments = _patient?.Treatments;
			var id = _patient?.Id;
			var update = _patient != null;

			_patient = new PatientDto
			{
				Id = id ?? 0,
				Address = new Address
				{
					Street = txtStrasse.Text,
					StreetNumber = txtHausNummer.Text,
					Postalcode = txtPlz.Text,
					City = txtOrt.Text,
					Country = "Schland"
				},
				Insurance = txtKrankenkasse.Text,
				InsuranceNumber = txtVersicherungsnummer.Text,
				Surname = txtVorname.Text,
				Name = txtNachname.Text,
				DateOfBirth = dtpGeburtsdatum.Value,
				Phone = txtTelefon.Text,
				Description = txtBesonderheit.Text,
				Salutation = (Salutation) comboBoxAnrede.SelectedValue,
				Treatments = existingTreatments ?? new List<TreatmentDto>()
			};

			if (update)
            {
	            await _patientService.UpdatePatientAsync(_patient);
            }
            else
            {
	            await _patientService.AddPatientAsync(_patient);
			}
		}

		private bool ValidateForm()
		{
			return !(string.IsNullOrWhiteSpace(txtKrankenkasse.Text) || 
			       string.IsNullOrWhiteSpace(txtVersicherungsnummer.Text) ||
                   string.IsNullOrWhiteSpace(txtVorname.Text) ||
                   string.IsNullOrWhiteSpace(txtNachname.Text) ||
                   string.IsNullOrWhiteSpace(txtStrasse.Text) ||
                   string.IsNullOrWhiteSpace(txtHausNummer.Text) ||
                   string.IsNullOrWhiteSpace(txtPlz.Text) ||
                   string.IsNullOrWhiteSpace(txtOrt.Text));
		}

		private GridTreatment GetSelectedBehandlung()
		{
			if (dataGridViewBehandlungen.SelectedRows.Count != 1)
				return null;

			return (GridTreatment)dataGridViewBehandlungen.SelectedRows[0].DataBoundItem;
		}

		private async Task RemoveTreatment(GridTreatment gridTreatment)
		{
			if (gridTreatment == null)
				return;

			var dialogResult = MessageBox.Show($"Möchten Sie die Behandlung am {gridTreatment.Date:dd.MM.yyyy} von \"{_patient.Surname} {_patient.Name}\" wirklich löschen?", "Behandlung löschen?", MessageBoxButtons.YesNo);
			if (dialogResult == DialogResult.Yes)
            {
                await _treatmentRepository.DeleteTreatmentAsync(gridTreatment.Id);
				_patient.Treatments.RemoveAll(x => x.Id == gridTreatment.Id);
				UpdateGrid();
			}

		}

		public void AddTreatment(TreatmentDto treatmentDto)
		{
			if (_patient == null)
				return;

			_patient.Treatments.Add(treatmentDto);
		}

		public void UpdateGrid()
		{
			if (_patient?.Treatments == null)
				return;

			var treatments = _patient.Treatments.Select(GetGridBehandlung).ToList();
			dataGridViewBehandlungen.DataSource = treatments;
			dataGridViewBehandlungen.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
		}

		private GridTreatment GetGridBehandlung(TreatmentDto treatmentDto)
		{
			return new GridTreatment
			{
				Id = treatmentDto.Id,
				Date = treatmentDto.Date,
				Misc = treatmentDto.Misc,
				Checkup = treatmentDto.Checkup
			};
		}

		private void OpenBehandlungForm(TreatmentDto treatmentDto)
		{
			var treatmentForm = new FormTreatment(this, treatmentDto);
            treatmentForm.Show();
		}

		private void btnAdd_Click(object sender, EventArgs e)
		{
			ClearErrorMessage();

			if (_patient == null)
			{
				labelError.Text = "Der Patient muss erst gespeichert werden.";
				return;
			}

			OpenBehandlungForm(null);
		}

		private async void btnDelete_Click(object sender, EventArgs e)
		{
			ClearErrorMessage();

			var selectedTreatment = GetSelectedBehandlung();

			if(selectedTreatment == null)
			{
				labelError.Text = "Bitte wählen Sie eine Behandlung aus.";
				return;
			}

			await RemoveTreatment(selectedTreatment);
		}

		private void btnEdit_Click(object sender, EventArgs e)
		{
			ClearErrorMessage();

			var gridTreatment = GetSelectedBehandlung();
			var treatment = _patient.Treatments.FirstOrDefault(x => x.Id == gridTreatment?.Id);

			if (treatment == null)
			{
				labelError.Text = "Bitte wählen Sie eine Behandlung aus.";
				return;
			}


			OpenBehandlungForm(treatment);
		}

		private void ClearErrorMessage()
		{
			labelError.Text = string.Empty;
		}
    }
}
